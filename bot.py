import re
import telebot
import sympy

# Get bot token from a file
with open("token.txt", "r") as token_file:
    token = token_file.read().rstrip()

# Initialise the bot
bot = telebot.TeleBot(token)

# Render a TeX expression
def render(matches):
    for match in matches:
        try:
            sympy.preview(
                match,
                viewer="file",
                filename="render.png",
                dvioptions=["-D", "600"],
                euler=False,
                preamble=r"""\documentclass{standalone}
                                         \usepackage{amsmath, amssymb}
                                         \begin{document}""",
            )
            return True
        except:
            return False


# Send the render
def send_render(matches, message):
    if message.chat.type == "group":
        reply = message.message_id
    else:
        reply = None

    if render(matches):
        with open("render.png", "rb") as image:
            bot.send_photo(message.chat.id, image, reply_to_message_id=reply)
    else:
        bot.send_message(
            message.chat.id, "Incorrect TeX expression!", reply_to_message_id=reply
        )


# React to inline TeX expressions
@bot.message_handler(regexp=r"\$[^$]+\$")
def inline(message):
    matches = re.findall(r"\$[^$]+\$", message.text)
    send_render(matches, message)


# React to standalone TeX expressions
@bot.message_handler(commands=["render"])
def standalone(message):
    expression = [
        " ".join(message.text.split()[1:])
    ]  # This removes the '/render' part from the message
    send_render(expression, message)


# Start the bot
bot.polling()
