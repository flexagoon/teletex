[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![License: MIT](https://img.shields.io/badge/License-MIT-black)](https://gitlab.com/flexagoon/teletex/-/blob/main/LICENSE)

# TeleTeX

TeleTeX is a simple Telegram bot for rendering TeX math expressions. The bot is located at https://t.me/teletex_bot, but it's currently not hosted anywhere, so it is offline.

# Usage

To use the bot, just send it a message containing one or multiple TeX expressions enclosed in dollar symbols (e.g. `$3x=2$`). Message can contain any other text, for example _"If \$3x=3\$, then \$x=1\$"_.  
Another way to use the bot is to use the `/render` command, followed by a TeX expression, like this: _/render 3x=3_.

# Self-hosting

## Dependencies

In order to launch the bot, you need to install `pytelegrambotapi` and `sympy` from PyPI. You also need a _FULL_ distribution of TeXLive installed on your machine.

## Launching

Create a file called `token.txt` in the bot directory and place your bot token inside. Run the `bot.py` file.

# TODO

- [x] Use amsmath LaTeX package
- [x] Make the bot work in group chats
- [ ] Make the bot asynchronous
- [ ] Find a hosting ;)
