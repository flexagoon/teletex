{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell {
  nativeBuildInputs = [
    python3
    python3Packages.pytelegrambotapi
    python3Packages.sympy
  ];
}
